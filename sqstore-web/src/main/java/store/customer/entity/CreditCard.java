package store.customer.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;

@Entity
public class CreditCard {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min=16, max=16)
    private String number;

    @Min(1)
    @Max(12)
    private int expiredMonth;
    
    @Digits(integer = 2, fraction = 0)
    private int expiredYear;

    private String name;
}
